<?php

// configure your app for the production environment

$app['twig.path'] = array(__DIR__.'/../templates');
$app['twig.options'] = array('cache' => __DIR__.'/../var/cache/twig');

$app['db.options'] = [
  'driver'   => 'pdo_mysql',
  'charset' => 'utf8',
  'path'     => __DIR__.'/app.db',
  'password' => 'password',
  'dbname' => 'dbname'
];