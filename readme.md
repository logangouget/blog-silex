﻿GOUGET Logan

# Rapport d'utilisation : Silex

  - [Introduction](#introduction)
    - [Silex](#silex)
    - [Le projet](#le-projet)
    - [Outils utilisés](#outils-utilis%C3%A9s)
  - [Synopsis des routes](#synopsis-des-routes)
    - [Routes publiques](#routes-publiques)
    - [Routes administrateur](#routes-administrateur)
    - [Route api](#route-api)
  - [Réalisation du projet](#r%C3%A9alisation-du-projet)
    - [Installation](#installation)
    - [Premiers tests](#premiers-tests)
    - [Contrôleurs](#contr%C3%B4leurs)
    - [Connexion à la base de données](#connexion-%C3%A0-la-base-de-donn%C3%A9es)
    - [Passer des données à la vue](#passer-des-donn%C3%A9es-%C3%A0-la-vue)
    - [Parcourir les données dans le template](#parcourir-les-donn%C3%A9es-dans-le-template)
    - [Envoyer une réponse au format JSON](#envoyer-une-r%C3%A9ponse-au-format-json)
  - [Conclusion](#conclusion)

## Introduction
### Silex
Silex est un micro-framework PHP basé sur des composants Symfony. Il permet de construire une application avec les outils essentiels. Bien entendu, il est possible d'ajouter d'autres composants au fur et à mesure du développement.

### Le projet
L'objectif de ce projet est de concevoir un système de blog basique dans lequel nous pourrons effectuer les actions suivantes :

 - Consulter une page regroupant l'ensemble des articles
 - Consulter les articles individuellement
 - Ajouter ou supprimer des articles
 - Editer un article
 
### Outils utilisés
Pour ce projet, j'utilise le moteur de template Twig, le framework CSS Spectre et une base de données MySQL.

## Synopsis des routes
### Routes publiques
> GET domaine/

Cette route retournera une vue Twig comme page d'accueil.

>GET domaine/blog

Ici, on retourne une vue qui affiche l'ensemble des articles.

>GET domaine/blog/{slug}

Dans ce cas, on retourne une vue qui affiche un seul article en fonction du slug transmis dans l'URL.

### Routes administrateur

>GET domaine/admin

On retourne une vue qui affiche un tableau contenant l'ensemble des articles à éditer/supprimer

>GET domaine/admin/edit/{slug}

On retourne une vue qui affiche un formulaire vide. On viendra par la suite remplir ce formulaire en JavaScript avec des requêtes AJAX. 

>POST domaine/admin/edit/{slug}

Cette route traitera via un contrôleur les requêtes de type POST et modifiera l'article en question.

>GET domaine/admin/add

On retourne une vue qui affiche un formulaire pour ajouter un article. Ce formulaire pourra faire une requête de type POST pour ajouter l'article.

>POST domaine/article/add

Cette route traitera via un contrôleur les requêtes de type POST et ajoutera un nouvel article.

>POST domaine/article/delete/{slug}

Cette route traitera via un contrôleur les requêtes de type POST et supprimera l'article demandé.

### Route api

>GET domaine/api/blog/{slug}

Cette route sera utilisée pour pré-remplir les champs du formulaire d'édition avec une requête AJAX.



## Réalisation du projet

### Installation
L'installation s'est déroulée sans aucun problème via le gestionnaire de dépendances composer présent sur les machines.

### Premiers tests
Ayant installé “Silex-Skeleton”, je me suis rendu sur la documentation github pour connaître les commandes permettant de lancer le serveur web. (Cette commande est présente dans la partie script de composer)

Notre dossier web contient deux index dont un pour le développement. Une barre de debug est présente en bas de notre page web et nous donne des informations.

Les vues ne sont pas compilés dans le fichier index.php mais dans le index_dev.php (pour le moment)

### Contrôleurs
Les contrôleurs sont présents dans un fichier **Controllers.php**

Chaque contrôleur est connecté à une route et à une action (get/post).
Pour de simples pages dans lesquels nous n'avons pas besoin de récupérer de données, il suffit de renvoyer un template **Twig**.

Pour une meilleure gestion des contrôleurs, il est plus simple de les regrouper dans différents fichiers. Pour cela, je crée un dossier Http/Controller contenant l'ensemble de mes fichiers contrôleurs. 
J'y ajoute **BlogController** et **AdminController.php**. Dans chacun de ces fichiers, je prends soin d'importer le nécessaire.

> use Silex\Application;
> use Symfony\Component\HttpFoundation\Request;
> use Symfony\Component\HttpFoundation\Response;

### Connexion à la base de données

Premièrement, il faut installer doctrine via composer puis enregistrer le service dans le fichier **app.php**. On y ajoute les informations de connexion à la base de données.

On peut ensuite se servir de **$app['db']** pour faire des requêtes sur la base de données.

### Passer des données à la vue
Pour passer les données, il suffit de mettre un tableau en deuxième argument de la méthode **render**. 

### Parcourir les données dans le template
Twig nous offre la possibilité de faire une boucle. Pour afficher les posts par exemple, il suffit de boucler sur une variables **$posts**.
### Envoyer une réponse au format JSON
Silex nous offre la possibilité de renvoyer une réponse au format JSON en une simple ligne : 

> return  $app->json(données, code de statut);

C'est avec cette méthode que l'on peut exploiter les données avec une requête AJAX.

## Conclusion
L'utilisation d'un micro-framework peut être bénéfique dans beaucoup de cas car nous installons que ce qui est nécessaire dans le cadre du projet. Cependant, toutes les tâches d'installation et de configuration des services demandent du temps !


