<?php

namespace Http\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class BlogController {

  public function show(Application $app){
    $sql = "SELECT * FROM Articles";

    $posts = $app['db']->fetchAll($sql);

    return $app['twig']->render('blog/index.html.twig', array(
      'posts' => $posts
    ));   
  }

  public function single(Application $app, $slug){
    $sql = "SELECT * FROM Articles WHERE slug = ?";
    $post = $app['db']->fetchAssoc($sql, array((string) $slug));

    return $app['twig']->render('blog/single.html.twig', array(
      'post' => $post
    ));   
  }

  public function singleJson(Application $app, $slug){
    $sql = "SELECT * FROM Articles WHERE slug = ?";
    $post = $app['db']->fetchAssoc($sql, array((string) $slug));

    if(!$post){
      return $app->json("Le post à éditer n'a pas été trouvé !", 404);
    }

    return $app->json($post, 200);
  }

}