<?php

namespace Http\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController {

  public function index(Application $app){
    $sql = "SELECT * FROM Articles";
    $posts = $app['db']->fetchAll($sql);

    return $app['twig']->render('admin/index.html.twig', array(
        'posts' => $posts
    ));    

  }

  public function createForm(Application $app){
    return $app['twig']->render('admin/create.html.twig');  
  }

  public function create(Application $app, Request $request){
    $app['db']->insert('Articles', array(
      'titre' => $request->request->get('title'),
      'auteur' => $request->request->get('author'),
      'slug' => $app['slugify']->slugify($request->request->get('title')),
      'contenu' => $request->request->get('content'),
      'date' => date("y-m-d")
    ));

    return $app['twig']->render('admin/create.html.twig');  
  }

  public function delete(Application $app, $slug){
    $app['db']->delete('Articles', array('slug' => $slug));

    return $app->redirect('/index_dev.php/admin');
  }

  public function editForm(Application $app, $slug){

    return $app['twig']->render('admin/edit.html.twig', array(
      'slug' => $slug
    ));   
  }

  public function edit(Application $app, Request $request, $slug){
    
    $sql = "UPDATE `Articles` SET titre = :title, auteur = :author, contenu = :content, slug = :newslug WHERE slug = :slug";

    $newslug = $app['slugify']->slugify($request->request->get('title'));

    $result = $app['db']->executeUpdate(
        $sql,
        array(
            'title' => $request->request->get('title'),
            'author' => $request->request->get('author'),
            'content' => $request->request->get('content'),
            'slug' => $slug,
            'newslug' => $newslug
        )
    );

    return $app->json("Votre article a été modifié !", 200);  
  }
}