<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', array());
})
->bind('homepage');

$app->get('/blog/', 'Http\Controller\BlogController::show')->bind('blog');

$app->get('/blog/{slug}', 'Http\Controller\BlogController::single');

$app->get('/admin', 'Http\Controller\AdminController::index')->bind('admin');

$app->get('/admin/edit/{slug}', 'Http\Controller\AdminController::editForm');

$app->post('/admin/edit/{slug}', 'Http\Controller\AdminController::edit');

$app->get('/admin/add', 'Http\Controller\AdminController::createForm')->bind('addArticle');

$app->post('/admin/add', 'Http\Controller\AdminController::create');

$app->post('/admin/delete/{slug}', 'Http\Controller\AdminController::delete');

// API ROUTES

$app->get('/api/blog/{slug}', 'Http\Controller\BlogController::singleJson');


$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});